package MusikplaylistConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import javax.swing.JLabel;

public class MusikplaylistConnection {

	private String driver = "com.mysql.jdbc.Driver";
	private String url = "jdbc:mysql://localhost/musikplaylist";
	private String user = "root";
	private String password = "";
	
	public boolean insertGaeste (String name) {
		String sql = "INSERT INTO t_gaeste (name) VALUES ('"+name+"');";
		
		try {
			//JDBC-Treiber laden
			Class.forName(driver);
			//Verbindung aufbauen
			Connection con = DriverManager.getConnection(url, user, password);
			
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			
			con.close();
		} catch (Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean insertMusik (String bandname, String titel, String genre) {
		String sql = "INSERT INTO t_musik (Bandname, Titel, Genre) VALUES ('" +bandname+ "', '" +titel+ "', '"+genre+"');";
		
		try {
			//JDBC-Treiber laden
			Class.forName(driver);
			//Verbindung aufbauen
			Connection con = DriverManager.getConnection(url, user, password);
			
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			
			con.close();
		} catch (Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean insertVotes (int f_lied_id) {
		String sql = "INSERT INTO t_votes (F_Lied_ID) VALUES ('" +f_lied_id+ "');";
		
		try {
			//JDBC-Treiber laden
			Class.forName(driver);
			//Verbindung aufbauen
			Connection con = DriverManager.getConnection(url, user, password);
			
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			
			con.close();
		} catch (Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean getSongs (JLabel titelname) {
		String sql = "SELECT DISTINCT CONCAT_WS(' - ', Bandname, Titel) AS Songs, Genre FROM `t_musik` LEFT JOIN t_votes ON t_musik.P_Lied_ID = t_votes.F_Lied_ID ORDER BY (SELECT COUNT(t_votes.F_Lied_ID) FROM t_votes WHERE t_votes.F_Lied_ID = t_musik.P_Lied_ID) DESC;";
		
		try {
			//JDBC-Treiber laden
			Class.forName(driver);
			//Verbindung aufbauen
			Connection con = DriverManager.getConnection(url, user, password);
			
			Statement stmt = con.createStatement();
			stmt.executeQuery(sql);
			stmt.getResultSet();
			
			con.close();
		} catch (Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
}