package MusikplaylistConnection;

import MusikplaylistConnection.MusikplaylistConnection;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;

public class MusikplaylistGUI extends JFrame {

	private JPanel contentPane;
	private static JTextField txt_Name;
	private static JTextField txt_Titelname;
	private static JTextField txt_Bandname;
	private static JTextField txt_Genre;
	Font quicksand; 									// Erstellt Costom Font aus Google Fonts
	private JTable table_MusikListe;

	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the frame.
	 */
	public MusikplaylistGUI() {
		MusikplaylistConnection dbc = new MusikplaylistConnection();
		setForeground(Color.BLACK);
		setFont(new Font("Arial", Font.PLAIN, 16));
		setTitle("Sound of Axe and Fire \uD83D\uDD25");
		
		try {
			quicksand = Font.createFont(Font.TRUETYPE_FONT,new File("Quicksand-Regular.otf")).deriveFont(30f);			//Erstelle die Funktionalität meiner Costom Font
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT,new File("Quicksand-Regular.otf")));
			
		} catch (IOException | FontFormatException e) {
			// TODO: handle exception
		}
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 513, 302);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 128, 128));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout(0, 0));
		
		JPanel Login = new JPanel();
		Login.setBackground(new Color(255, 255, 255));
		contentPane.add(Login, "name_360397423499000");
		Login.setLayout(new BorderLayout(0, 0));
		
		JPanel Oben = new JPanel();
		Oben.setBackground(new Color(0, 128, 128));
		Login.add(Oben, BorderLayout.NORTH);
		Oben.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lbl_Titel = new JLabel("Sound of Axe and Fire");
		lbl_Titel.setFont(new Font("quicksand", Font.PLAIN, 26));
		lbl_Titel.setForeground(new Color(255, 255, 255));
		Oben.add(lbl_Titel);
		
		JPanel Mitte = new JPanel();
		Mitte.setBackground(new Color(0, 128, 128));
		Login.add(Mitte, BorderLayout.CENTER);
		GridBagLayout gbl_Mitte = new GridBagLayout();
		gbl_Mitte.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_Mitte.rowHeights = new int[]{0, 0, 0, 0};
		gbl_Mitte.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_Mitte.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		Mitte.setLayout(gbl_Mitte);
		
		Component verticalStrut = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
		gbc_verticalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut.gridx = 1;
		gbc_verticalStrut.gridy = 0;
		Mitte.add(verticalStrut, gbc_verticalStrut);
		
		Component verticalStrut_1 = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut_1 = new GridBagConstraints();
		gbc_verticalStrut_1.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut_1.gridx = 5;
		gbc_verticalStrut_1.gridy = 0;
		Mitte.add(verticalStrut_1, gbc_verticalStrut_1);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut.gridx = 0;
		gbc_horizontalStrut.gridy = 1;
		Mitte.add(horizontalStrut, gbc_horizontalStrut);
		
		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_3 = new GridBagConstraints();
		gbc_horizontalStrut_3.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_3.gridx = 3;
		gbc_horizontalStrut_3.gridy = 1;
		Mitte.add(horizontalStrut_3, gbc_horizontalStrut_3);
		
		JLabel lbl_Name = new JLabel("Name");
		lbl_Name.setFont(new Font("Arial", Font.PLAIN, 18));
		lbl_Name.setForeground(new Color(255, 255, 255));
		GridBagConstraints gbc_lbl_Name = new GridBagConstraints();
		gbc_lbl_Name.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_Name.anchor = GridBagConstraints.EAST;
		gbc_lbl_Name.gridx = 4;
		gbc_lbl_Name.gridy = 1;
		Mitte.add(lbl_Name, gbc_lbl_Name);
		
		txt_Name = new JTextField("Username");
		txt_Name.setFont(new Font("Arial", Font.PLAIN, 18));
		GridBagConstraints gbc_txt_Name = new GridBagConstraints();
		gbc_txt_Name.insets = new Insets(0, 0, 5, 5);
		gbc_txt_Name.anchor = GridBagConstraints.WEST;
		gbc_txt_Name.gridx = 6;
		gbc_txt_Name.gridy = 1;
		Mitte.add(txt_Name, gbc_txt_Name);
		txt_Name.setColumns(10);
		
		JLabel lbl_Fehlerbericht = new JLabel("");
		lbl_Fehlerbericht.setFont(new Font("Arial", Font.PLAIN, 12));
		GridBagConstraints gbc_lbl_Fehlerbericht = new GridBagConstraints();
		gbc_lbl_Fehlerbericht.insets = new Insets(0, 0, 0, 5);
		gbc_lbl_Fehlerbericht.gridx = 6;
		gbc_lbl_Fehlerbericht.gridy = 2;
		Mitte.add(lbl_Fehlerbericht, gbc_lbl_Fehlerbericht);
		
		JPanel Unten = new JPanel();
		Unten.setBackground(new Color(0, 128, 128));
		Login.add(Unten, BorderLayout.SOUTH);
		Unten.setLayout(new GridLayout(0, 3, 0, 0));
		
		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		Unten.add(horizontalStrut_2);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		Unten.add(horizontalStrut_1);
		
		
		JPanel SongsEingeben = new JPanel();
		SongsEingeben.setBackground(new Color(0, 128, 128));
		contentPane.add(SongsEingeben, "name_364413089040800");
		SongsEingeben.setLayout(new BorderLayout(0, 0));
		
		JButton btn_Login = new JButton("Login");
		btn_Login.setActionCommand("Login");
		btn_Login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String LoginName = txt_Name.getText();															//Name wird von Gast in einer Variable gespeichert
				if(LoginName.equals(""))																//wenn Name nicht eingetragen wird,
					txt_Name.setBackground(Color.RED);															//wird Feld rot
				else{
					txt_Name.setBackground(Color.WHITE);
					((CardLayout) contentPane.getLayout()).next(contentPane);
				}
				lbl_Fehlerbericht.setText("Geben Sie bitte Ihren Namen ein!!!");
				dbc.insertGaeste(LoginName);
			}
			
			
			
		});
		btn_Login.setForeground(new Color(0, 128, 128));
		btn_Login.setFont(new Font("Arial", Font.PLAIN, 18));
		Unten.add(btn_Login);
		
		
		
		JPanel Oben2 = new JPanel();
		Oben2.setForeground(new Color(255, 255, 255));
		Oben2.setBackground(new Color(0, 128, 128));
		SongsEingeben.add(Oben2, BorderLayout.NORTH);
		
		JLabel lbl_Titel_1 = new JLabel("Sound of Axe and Fire");
		lbl_Titel_1.setForeground(Color.WHITE);
		lbl_Titel_1.setFont(new Font("quicksand", Font.PLAIN, 26));
		Oben2.add(lbl_Titel_1);
		
		JPanel EintrageFläche = new JPanel();
		EintrageFläche.setForeground(new Color(255, 255, 255));
		EintrageFläche.setBackground(new Color(0, 128, 128));
		SongsEingeben.add(EintrageFläche, BorderLayout.CENTER);
		GridBagLayout gbl_EintrageFläche = new GridBagLayout();
		gbl_EintrageFläche.columnWidths = new int[]{0, 0, 0};
		gbl_EintrageFläche.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
		gbl_EintrageFläche.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_EintrageFläche.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		EintrageFläche.setLayout(gbl_EintrageFläche);
		
		JLabel lbl_Titelname = new JLabel("Titelname");
		lbl_Titelname.setForeground(new Color(255, 255, 255));
		lbl_Titelname.setFont(new Font("Arial", Font.PLAIN, 18));
		GridBagConstraints gbc_lbl_Titelname = new GridBagConstraints();
		gbc_lbl_Titelname.anchor = GridBagConstraints.EAST;
		gbc_lbl_Titelname.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_Titelname.gridx = 0;
		gbc_lbl_Titelname.gridy = 1;
		EintrageFläche.add(lbl_Titelname, gbc_lbl_Titelname);
		
		txt_Titelname = new JTextField();
		txt_Titelname.setFont(new Font("Arial", Font.PLAIN, 18));
		GridBagConstraints gbc_txt_Titelname = new GridBagConstraints();
		gbc_txt_Titelname.anchor = GridBagConstraints.WEST;
		gbc_txt_Titelname.insets = new Insets(0, 0, 5, 0);
		gbc_txt_Titelname.gridx = 1;
		gbc_txt_Titelname.gridy = 1;
		EintrageFläche.add(txt_Titelname, gbc_txt_Titelname);
		txt_Titelname.setColumns(10);
		
		JLabel lbl_Bandname = new JLabel("Bandname");
		lbl_Bandname.setForeground(new Color(255, 255, 255));
		lbl_Bandname.setFont(new Font("Arial", Font.PLAIN, 18));
		GridBagConstraints gbc_lbl_Bandname = new GridBagConstraints();
		gbc_lbl_Bandname.anchor = GridBagConstraints.EAST;
		gbc_lbl_Bandname.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_Bandname.gridx = 0;
		gbc_lbl_Bandname.gridy = 2;
		EintrageFläche.add(lbl_Bandname, gbc_lbl_Bandname);
		
		txt_Bandname = new JTextField();
		txt_Bandname.setFont(new Font("Arial", Font.PLAIN, 18));
		GridBagConstraints gbc_txt_Bandname = new GridBagConstraints();
		gbc_txt_Bandname.anchor = GridBagConstraints.WEST;
		gbc_txt_Bandname.insets = new Insets(0, 0, 5, 0);
		gbc_txt_Bandname.gridx = 1;
		gbc_txt_Bandname.gridy = 2;
		EintrageFläche.add(txt_Bandname, gbc_txt_Bandname);
		txt_Bandname.setColumns(10);
		
		JLabel lbl_Genre = new JLabel("Genre");
		lbl_Genre.setForeground(new Color(255, 255, 255));
		lbl_Genre.setFont(new Font("Arial", Font.PLAIN, 18));
		GridBagConstraints gbc_lbl_Genre = new GridBagConstraints();
		gbc_lbl_Genre.anchor = GridBagConstraints.EAST;
		gbc_lbl_Genre.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_Genre.gridx = 0;
		gbc_lbl_Genre.gridy = 3;
		EintrageFläche.add(lbl_Genre, gbc_lbl_Genre);
		
		txt_Genre = new JTextField();
		txt_Genre.setFont(new Font("Arial", Font.PLAIN, 18));
		GridBagConstraints gbc_txt_Genre = new GridBagConstraints();
		gbc_txt_Genre.insets = new Insets(0, 0, 5, 0);
		gbc_txt_Genre.anchor = GridBagConstraints.WEST;
		gbc_txt_Genre.gridx = 1;
		gbc_txt_Genre.gridy = 3;
		EintrageFläche.add(txt_Genre, gbc_txt_Genre);
		txt_Genre.setColumns(10);
		
		Component verticalStrut_2 = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut_2 = new GridBagConstraints();
		gbc_verticalStrut_2.insets = new Insets(0, 0, 0, 5);
		gbc_verticalStrut_2.gridx = 0;
		gbc_verticalStrut_2.gridy = 4;
		EintrageFläche.add(verticalStrut_2, gbc_verticalStrut_2);
		
		Component verticalStrut_3 = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut_3 = new GridBagConstraints();
		gbc_verticalStrut_3.gridx = 1;
		gbc_verticalStrut_3.gridy = 4;
		EintrageFläche.add(verticalStrut_3, gbc_verticalStrut_3);
		
		JPanel Add = new JPanel();
		Add.setBackground(new Color(0, 128, 128));
		SongsEingeben.add(Add, BorderLayout.SOUTH);
		Add.setLayout(new GridLayout(0, 3, 0, 0));
		
		Component horizontalStrut_2_1 = Box.createHorizontalStrut(20);
		Add.add(horizontalStrut_2_1);
		
		Component horizontalStrut_1_1 = Box.createHorizontalStrut(20);
		Add.add(horizontalStrut_1_1);
		
		JButton btn_Login_1 = new JButton("Add To List");
		btn_Login_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String songTitel = txt_Titelname.getText();
				String bandName = txt_Bandname.getText();														// Wunsch daten von Gast in einer Variable gespeichert
				String genre = txt_Genre.getText();																
				
				
				if(songTitel.equals(""))															//wenn daten nicht eingetragen wird,
					txt_Titelname.setBackground(Color.RED);														//wird Feld rot
				else{
					txt_Titelname.setBackground(Color.WHITE);
				}
				
				if(bandName.equals(""))															//wenn daten nicht eingetragen wird,
					txt_Bandname.setBackground(Color.RED);														//wird Feld rot
				else{
					txt_Bandname.setBackground(Color.WHITE);
				}
				if(genre.equals(""))																//wenn daten nicht eingetragen wird,
					txt_Genre.setBackground(Color.RED);															//wird Feld rot
				else{
					txt_Genre.setBackground(Color.WHITE);
					((CardLayout) contentPane.getLayout()).next(contentPane);
				}	
				dbc.insertMusik(songTitel, bandName, genre);

			}
		});
		btn_Login_1.setForeground(new Color(0, 128, 128));
		btn_Login_1.setFont(new Font("Arial", Font.PLAIN, 18));
		Add.add(btn_Login_1);
		
		JPanel SongsVoten = new JPanel();
		SongsVoten.setBackground(new Color(0, 128, 128));
		contentPane.add(SongsVoten, "name_365856215547600");
		SongsVoten.setLayout(new BorderLayout(0, 0));
		
		JPanel Top = new JPanel();
		Top.setBackground(new Color(0, 128, 128));
		SongsVoten.add(Top, BorderLayout.NORTH);
		
		JLabel lbl_Titel_2 = new JLabel("Sound of Axe and Fire");
		lbl_Titel_2.setForeground(Color.WHITE);
		lbl_Titel_2.setFont(new Font("Dialog", Font.PLAIN, 26));
		Top.add(lbl_Titel_2);
		
		JPanel Liste = new JPanel();
		Liste.setForeground(new Color(255, 255, 255));
		Liste.setBackground(new Color(0, 128, 128));
		SongsVoten.add(Liste, BorderLayout.CENTER);
		GridBagLayout gbl_Liste = new GridBagLayout();
		gbl_Liste.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_Liste.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_Liste.columnWeights = new double[]{1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_Liste.rowWeights = new double[]{1.0, 1.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		Liste.setLayout(gbl_Liste);
		
		
		String[] columnNames = { "ID", "Titelname", "Bandname", "Genre", "Vote" };
		int maxVote = 10;																//Variable dient später zur auslesung der Max. Votes
		Object[][] data = new Object[maxVote][maxVote];									//Erstellt die Tabelle in den Maßen
				for(int i = 0; i < data.length; i++)
					for(int j = 0; j < data.length; j++)
						data[i][j] = "1";
				
		JTable table_MusikListe = new JTable(data,columnNames);
		table_MusikListe.setRowSelectionAllowed(true);
		GridBagConstraints gbc_table_MusikListe = new GridBagConstraints();
		gbc_table_MusikListe.gridheight = 6;
		gbc_table_MusikListe.gridwidth = 7;
		gbc_table_MusikListe.insets = new Insets(0, 0, 5, 5);
		gbc_table_MusikListe.fill = GridBagConstraints.BOTH;
		gbc_table_MusikListe.gridx = 0;
		gbc_table_MusikListe.gridy = 0;
		Liste.add(table_MusikListe, gbc_table_MusikListe);
		
	//	Liste.add(new JScrollPane(table_MusikListe));
		
		
		
		
		
		JPanel Vote = new JPanel();
		Vote.setBackground(new Color(0, 128, 128));
		SongsVoten.add(Vote, BorderLayout.SOUTH);
		Vote.setLayout(new GridLayout(0, 3, 0, 0));
		
		Component horizontalStrut_2_1_1 = Box.createHorizontalStrut(20);
		Vote.add(horizontalStrut_2_1_1);
		
		JButton btn_Login_1_1 = new JButton("Show Results");
		btn_Login_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//dbc.getSongs(titelname);
				((CardLayout) contentPane.getLayout()).next(contentPane);
			}
		});
		
		JButton btn_VoteYourSong = new JButton("Vote");
		btn_VoteYourSong.setForeground(new Color(0, 128, 128));
		btn_VoteYourSong.setFont(new Font("Arial", Font.PLAIN, 18));
		Vote.add(btn_VoteYourSong);
		btn_VoteYourSong.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {														//Vote wird von Gast in einer Variable gespeichert
						
				//int a = Integer.parseInt(txt_VoteYourSong.getText());	
				//dbc.insertVotes(a);
				
			}
		});
		btn_Login_1_1.setForeground(new Color(0, 128, 128));
		btn_Login_1_1.setFont(new Font("Arial", Font.PLAIN, 18));
		Vote.add(btn_Login_1_1);
		
		JPanel SongErgebnise = new JPanel();
		contentPane.add(SongErgebnise, "name_366955711651900");
		SongErgebnise.setLayout(new BorderLayout(0, 0));
		
		JPanel Titel = new JPanel();
		Titel.setBackground(new Color(0, 128, 128));
		SongErgebnise.add(Titel, BorderLayout.NORTH);
		
		JLabel lbl_Titel_3 = new JLabel("Sound of Axe and Fire");
		lbl_Titel_3.setForeground(Color.WHITE);
		lbl_Titel_3.setFont(new Font("Dialog", Font.PLAIN, 26));
		Titel.add(lbl_Titel_3);
		
		JPanel Ergebnisse = new JPanel();
		Ergebnisse.setForeground(Color.WHITE);
		Ergebnisse.setBackground(new Color(0, 128, 128));
		SongErgebnise.add(Ergebnisse, BorderLayout.CENTER);
		GridBagLayout gbl_Ergebnisse = new GridBagLayout();
		gbl_Ergebnisse.columnWidths = new int[]{0, 0, 0, 0, 0, 0};
		gbl_Ergebnisse.rowHeights = new int[]{0, 0};
		gbl_Ergebnisse.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_Ergebnisse.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		Ergebnisse.setLayout(gbl_Ergebnisse);
		
		JLabel lbl_Titelname_2 = new JLabel("Titelname");
		lbl_Titelname_2.setForeground(Color.WHITE);
		lbl_Titelname_2.setFont(new Font("Arial", Font.PLAIN, 18));
		GridBagConstraints gbc_lbl_Titelname_2 = new GridBagConstraints();
		gbc_lbl_Titelname_2.insets = new Insets(0, 0, 0, 5);
		gbc_lbl_Titelname_2.gridx = 1;
		gbc_lbl_Titelname_2.gridy = 0;
		Ergebnisse.add(lbl_Titelname_2, gbc_lbl_Titelname_2);
		
		JLabel lbl_Bandname_2 = new JLabel("Bandname");
		lbl_Bandname_2.setForeground(Color.WHITE);
		lbl_Bandname_2.setFont(new Font("Arial", Font.PLAIN, 18));
		GridBagConstraints gbc_lbl_Bandname_2 = new GridBagConstraints();
		gbc_lbl_Bandname_2.insets = new Insets(0, 0, 0, 5);
		gbc_lbl_Bandname_2.gridx = 2;
		gbc_lbl_Bandname_2.gridy = 0;
		Ergebnisse.add(lbl_Bandname_2, gbc_lbl_Bandname_2);
		
		JLabel lbl_Genre_2 = new JLabel("Genre");
		lbl_Genre_2.setForeground(Color.WHITE);
		lbl_Genre_2.setFont(new Font("Arial", Font.PLAIN, 18));
		GridBagConstraints gbc_lbl_Genre_2 = new GridBagConstraints();
		gbc_lbl_Genre_2.insets = new Insets(0, 0, 0, 5);
		gbc_lbl_Genre_2.gridx = 3;
		gbc_lbl_Genre_2.gridy = 0;
		Ergebnisse.add(lbl_Genre_2, gbc_lbl_Genre_2);
		
		JLabel lbl_Votes = new JLabel("Votes");
		lbl_Votes.setForeground(Color.WHITE);
		lbl_Votes.setFont(new Font("Arial", Font.PLAIN, 18));
		GridBagConstraints gbc_lbl_Votes = new GridBagConstraints();
		gbc_lbl_Votes.gridx = 4;
		gbc_lbl_Votes.gridy = 0;
		Ergebnisse.add(lbl_Votes, gbc_lbl_Votes);
		
		JPanel Exit = new JPanel();
		Exit.setBackground(new Color(0, 128, 128));
		SongErgebnise.add(Exit, BorderLayout.SOUTH);
		Exit.setLayout(new GridLayout(0, 3, 0, 0));
		
		Component horizontalStrut_2_1_1_1 = Box.createHorizontalStrut(20);
		Exit.add(horizontalStrut_2_1_1_1);
		
		JButton btnGoBackTo = new JButton("Go Back to Login");
		btnGoBackTo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout) contentPane.getLayout()).next(contentPane);
			}
		});
		btnGoBackTo.setForeground(new Color(0, 128, 128));
		btnGoBackTo.setFont(new Font("Arial", Font.PLAIN, 16));
		Exit.add(btnGoBackTo);
		
		JButton btn_Login_2 = new JButton("Exit");
		btn_Login_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		btn_Login_2.setForeground(new Color(0, 128, 128));
		btn_Login_2.setFont(new Font("Arial", Font.PLAIN, 18));
		Exit.add(btn_Login_2);
		
		
	}
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MusikplaylistGUI frame = new MusikplaylistGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
	
			}
		});
	}
}	